package com.example.nycschools.schoollist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.nycschools.models.School
import com.example.nycschools.repository.SchoolRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Harsha Konduru
 * View model to handle the logic for List of Schools.
 */

class SchoolListViewModel constructor(
    private val repository: SchoolRepository
): ViewModel() {

    /**
     * Notifies whether to show loading or not.
     */
    private val _showLoading = MutableLiveData<Boolean>()
    val showLoading: LiveData<Boolean> = _showLoading

    /**
     * Notifies whether to show loading or not when trying to get more items using paging.
     */
    private val _showLoadingMoreProgressbar = MutableLiveData<Boolean>()
    val showLoadingMoreProgressbar: LiveData<Boolean> = _showLoadingMoreProgressbar

    /**
     * Notifies failure.
     */
    private val _failure = MutableLiveData<Boolean>()
    val failure: LiveData<Boolean> = _failure

    /**
     * Notifies success
     */
    private val _success = MutableLiveData<Boolean>()
    val success: LiveData<Boolean> = _success

    var schoolsList: LiveData<PagedList<School>> = MutableLiveData()

    init {
        loadListOfSchools()
    }

    private fun loadListOfSchools() {
        val pagedListConfig = PagedList.Config.Builder()
            .setPageSize(PAGE_LIMIT)
            .setInitialLoadSizeHint(PAGE_LIMIT)
            .setPrefetchDistance(PAGING_FETCH_DISTANCE)
            .setEnablePlaceholders(false)
            .build()

        val dataSourceFactory = object : DataSource.Factory<Int, School>() {
            override fun create(): DataSource<Int, School> {
                return SchoolListDataSource(
                    repository,
                    viewModelScope,
                    Dispatchers.IO,
                    PAGE_LIMIT,
                    INITIAL_OFFSET
                ) {
                    onPagingStateChanges(it)
                }
            }
        }

        schoolsList = LivePagedListBuilder(dataSourceFactory, pagedListConfig).build()
    }

    private fun onPagingStateChanges(state: PaginatingState) {
        when (state) {
            PaginatingState.INITIAL_LOADING -> _showLoading.postValue(true)
            PaginatingState.INITIAL_SUCCESS -> {
                _showLoading.postValue(false)
                _success.postValue(true)
            }
            PaginatingState.INITIAL_FAILURE -> {
                // Can show the Retry to make a call again if have time.
                _showLoading.postValue(false)
                _failure.postValue(true)
            }
            PaginatingState.AFTER_LOADING -> {
                _showLoadingMoreProgressbar.postValue(true)
            }
            PaginatingState.AFTER_SUCCESS, PaginatingState.AFTER_FAILURE -> {
                _showLoadingMoreProgressbar.postValue(false)
            }
        }
    }

    companion object {
        /**
         * Limit to show on the page.
         */
        private const val PAGE_LIMIT = 20

        private const val INITIAL_OFFSET = 0

        /**
         * When to start further loading from the end of the page.
         */
        private const val PAGING_FETCH_DISTANCE = 5
    }
}