package com.example.nycschools.schoollist

import androidx.paging.PageKeyedDataSource
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.repository.SchoolRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * Created by Harsha Konduru
 * Data Source to make an initial and subsequent Api calls while scrolling.
 */
class SchoolListDataSource(
    private val repository: SchoolRepository,
    private val coroutineScope: CoroutineScope,
    private val dispatcher: CoroutineDispatcher,
    private val limit: Int,
    private val initialOffset: Int,
    private val paginatingState: (state: PaginatingState) -> Unit
) : PageKeyedDataSource<Int, School>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, School>
    ) {

        val failureCallback = {
            paginatingState.invoke(PaginatingState.INITIAL_FAILURE)
        }
        val exceptionHandler = CoroutineExceptionHandler { _, _ -> failureCallback.invoke() }
        coroutineScope.launch(dispatcher + exceptionHandler) {
            paginatingState.invoke(PaginatingState.INITIAL_LOADING)
            val response = repository.getListOfSchools(limit, initialOffset)
            if (response.status == Status.SUCCESS) {
                val nextOffset = initialOffset + 1
                paginatingState.invoke(PaginatingState.INITIAL_SUCCESS)
                response.data?.let {
                    callback.onResult(it, null, nextOffset)
                } ?: kotlin.run {
                    // Can notify as No data available to display.
                    failureCallback.invoke()
                }
            } else {
                failureCallback.invoke()
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, School>) {
        val offset = params.key
        val exceptionHandler = CoroutineExceptionHandler { _, _ ->
            paginatingState.invoke(PaginatingState.AFTER_FAILURE)
        }
        coroutineScope.launch(dispatcher + exceptionHandler) {
            paginatingState.invoke(PaginatingState.AFTER_LOADING)
            val response = repository.getListOfSchools(limit, offset)
            if (response.status == Status.SUCCESS) {
                val nextOffset = offset + 1
                paginatingState.invoke(PaginatingState.AFTER_SUCCESS)
                response.data?.let { callback.onResult(it, nextOffset) } ?: kotlin.run {
                    // No data obtained, it means,there can be no items to paginate.
                    callback.onResult(emptyList(), null)
                }
            } else {
                paginatingState.invoke(PaginatingState.AFTER_FAILURE)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, School>) {
        // Not handling before currently.
    }
}

enum class PaginatingState {
    INITIAL_LOADING, INITIAL_SUCCESS, INITIAL_FAILURE,
    AFTER_LOADING, AFTER_SUCCESS, AFTER_FAILURE
}