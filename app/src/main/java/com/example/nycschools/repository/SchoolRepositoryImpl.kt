package com.example.nycschools.repository

import com.example.nycschools.api.ApiService
import com.example.nycschools.api.Resource
import com.example.nycschools.api.RetrofitProvider
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolDetails

/**
* Created by Harsha Konduru
*
* Implementation class for [SchoolRepository]
*/
class SchoolRepositoryImpl constructor(
    private val apiService: ApiService = RetrofitProvider.getInstance().getApiService()
): SchoolRepository {

    /**
     * Get list of schools
     */
    override suspend fun getListOfSchools(limit: Int, offset: Int): Resource<List<School>?> {
        val response = apiService.getListOfSchools(limit, offset)
        return if(response.isSuccessful) {
            Resource.success(response.body())
        } else{
            Resource.error(response.message())
        }
    }

    /**
     * Get details of a particular school based on the dbn.
     */
    override suspend fun getSchoolDetails(dbn: String): Resource<List<SchoolDetails>?> {
        val response = apiService.getSchoolDetails(dbn)
        return if(response.isSuccessful) {
            Resource.success(response.body())
        }
        else {
            Resource.error(response.message())
        }
    }
}