package com.example.nycschools.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Base url for the API
 */
private const val BASE_URL = "https://data.cityofnewyork.us/"

class RetrofitProvider {

    private var retrofit: Retrofit? = null

    private fun getRetrofit(): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!
    }

    fun getApiService(): ApiService {
        return getRetrofit().create(ApiService::class.java)
    }

    companion object {
        private var sInstance: RetrofitProvider? = null

        @JvmStatic
        fun getInstance(): RetrofitProvider {
            if (sInstance == null) {
                sInstance = RetrofitProvider()
            }
            return sInstance!!
        }
    }
}