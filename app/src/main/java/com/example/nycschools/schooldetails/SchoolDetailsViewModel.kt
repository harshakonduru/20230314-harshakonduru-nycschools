package com.example.nycschools.schooldetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolDetails
import com.example.nycschools.repository.SchoolRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

/**
 * Created by Harsha Konduru
 * View Model to handle logic for school details screen
 */

class SchoolDetailsViewModel constructor(
    private val repository: SchoolRepository,
    private val dispatcher: CoroutineDispatcher
): ViewModel() {

    /**
     * Notifies if no data is received.
     */
    private val _noDataReceived = MutableLiveData<Boolean>()
    val noDataReceived: LiveData<Boolean> = _noDataReceived

    /**
     * Notifies the school details.
     */
    private val _schoolDetails = MutableLiveData<Pair<School, SchoolDetails>>()
    val schoolDetails: LiveData<Pair<School, SchoolDetails>> = _schoolDetails

    /**
     * Provides the status of the API call.
     */
    private val _status = MutableLiveData<Status>()
    val status: LiveData<Status> = _status

    /**
     * Get the details of the school.
     * @param school instance of [School]
     */
    fun getDetails(school: School) {
        _status.postValue(Status.Waiting)
        val exceptionHandler = CoroutineExceptionHandler { _, _ ->
            _status.postValue(Status.ERROR)
        }
        viewModelScope.launch(dispatcher + exceptionHandler) {
            val response = repository.getSchoolDetails(school.dbn)
            if (response.status == Status.SUCCESS) {
                val details = response.data
                if (details.isNullOrEmpty()) {
                    _noDataReceived.postValue(true)
                } else {
                    // We can convert the response model to Domain objects that can provide data to UI.
                    val firstDetail = details.first()
                    _schoolDetails.postValue(school to firstDetail)
                }
            }
            _status.postValue(response.status)
        }
    }
}