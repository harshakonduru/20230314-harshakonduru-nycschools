package com.example.nycschools

import com.example.nycschools.api.ApiService
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolDetails
import com.example.nycschools.repository.SchoolRepository
import com.example.nycschools.repository.SchoolRepositoryImpl
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Response

/**
 * Created by Harsha Konduru
 */
@OptIn(ExperimentalCoroutinesApi::class)
class SchoolRepositoryImplTest {

    private val mockService = mockk<ApiService>(relaxed = true)
    private lateinit var repository: SchoolRepository

    @Before
    fun setUp() {
        repository = SchoolRepositoryImpl(mockService)
    }

    @Test
    fun `should return Resource as success with data when api successfully retrieves the list of schools`() =
        runTest {
            val mockResponse = listOf<School>(mockk(), mockk())
            coEvery {
                mockService.getListOfSchools(any(), any())
            } returns Response.success(mockResponse)
            val repository = SchoolRepositoryImpl(mockService)
            val response = repository.getListOfSchools(LIMIT, OFFSET)
            Assert.assertNotNull(response)
            Assert.assertEquals(Status.SUCCESS, response.status)
            Assert.assertEquals(2, response.data?.size)
        }

    @Test
    fun `should return Resource as Error with message when Api call is failed`() {
        coEvery {
            mockService.getListOfSchools(any(), any())
        } returns Response.error(
            400,
            ResponseBody.create(MediaType.parse("UTF-8"), "Failed to get Response")
        )
        runTest {
            val response = repository.getListOfSchools(LIMIT, OFFSET)
            Assert.assertNotNull(response)
            Assert.assertEquals(Status.ERROR, response.status)
        }
    }

    @Test
    fun `should return Resource as success with data when Api call successful retrieves school details`() {
        val mockList = listOf<SchoolDetails>(mockk())
        coEvery { mockService.getSchoolDetails(any()) } returns Response.success(mockList)
        runTest {
            val response = repository.getSchoolDetails(DBN)
            Assert.assertNotNull(response)
            Assert.assertEquals(Status.SUCCESS, response.status)
            Assert.assertEquals(1, response.data?.size)
        }
    }

    companion object {
        private const val LIMIT = 10
        private const val OFFSET = 0
        private const val DBN = "TEST_DBN"
    }
}